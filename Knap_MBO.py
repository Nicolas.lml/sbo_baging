
'''
    Este codigo contiene el algoritmo MBO enfocado a resolver el problema de la mochila

'''


import MBO 
import numpy as np


#Esta clase resuelve una instancia del problema de la mochila 
class Knap_MBO(MBO.MBO):
    
    #Esta funcion contiene la funcion real de evaluacion del problema
    def Evaluacion_Real(self,Individuo):
        #Aqui se calcula el fitness de combinacion. 
        valor = 0
        peso = 0
        for i in range(self.NumDim):
            if(Individuo[i]==1):
                peso += self.Datos[i][1]
                valor += self.Datos[i][0]
        #PENALIZACION 
        if(peso > self.Cap):
            valor -=   np.log2(((peso - self.Cap )* self.R) +1)
        
        return -valor   
    
    #En este metodo se inicializan la informacion faltante del problema de la mochila
    def Inicializacion(self,Datos,Capacidad):
        self.Datos = Datos
        self.Cap   = Capacidad
        self.NumDim = len(self.Datos)
        self.Dominio = [0,1]
        
        #R es un factor que se calcula para las penalizaciones
        self.R=0
        for i in range(len(self.Datos)):
            for j in range(len(self.Datos)):
                if(self.R < (self.Datos[i][0] / self.Datos[i][1])): self.R = self.Datos[i][0] / self.Datos[i][1]

        #Desde aqui
        #Se crea una primera poblacion aleatoria con la que se entrenara el modelo
        np.random.seed(self.Semilla)

        self.Input = np.random.randint(self.Dominio[0],self.Dominio[1]+1,(self.PobInc,self.NumDim))
 

        for i in range(self.PobInc):
            self.OutPut.append(self.Evaluacion_Real(self.Input[i]))