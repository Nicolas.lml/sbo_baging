

'''
    Este codigo ejecuta la experimentacion de 
        Instancia:   1-21 
        Modelo:      RedN, RBF, RForest, SVM, AdaBoo, DTree, krig, K-NN
        Guardar:     "Si", "No" guardar experimentacion 
        Semilla:     Semilla para el generador aleatorio
        
    Ejemplo de Ejecucion a traves de linea de comando
    
    python3 Demonio.py 1 RedN Si 30 Ruta
    
'''

import sys
import pandas as pd
import numpy as np

import Subset_MBO as SMBO



#Parametros de la Experimentacion
#................................................................................................................

#Especificaciones de la experimentacion
TypeVar = "Dis"
NumInst     = sys.argv[1]           #Numero de la instancia para experimentar
ModeloType  = sys.argv[2]           #Tipo de modelo que se utilizara
GenArchivos = sys.argv[3]           #Saber si se require guardar el archivo
Seed        = int(sys.argv[4])      #Semilla para el generador aleatorio
Ruta        = sys.argv[5]           #Ruta donde se guarda la informacion
RutInst     = sys.argv[6]           #Ruta donde se encuetran las instancias


#NumInst     = "KP1.csv"                #Numero de la instancia para experimentar
#ModeloType  = "Top1Sub"
#GenArchivos = "Si"                     #Saber si se require guardar el archivo
#Seed        = 1     #Semilla para el generador aleatorio
#Ruta        = "./RESULTADOS/"          #Ruta donde se guarda la informacion
#RutInst     = "./Instancias/"          #Ruta donde se encuetran las instancias


Ensambles = {
    "Top1Sub": ["RBF","KNN","SVM"],
    "Top6Sub": ["RBF","RandomF","Kriging","SVM","KNN","AdaBoost"],
    "Top4Sub": ["RBF","RandomF","Kriging","SVM"],
    "Top4Esp": ["RandomF","Kriging","KNN","AdaBoost"]
    }


#Parametros del algoritmo genetico
NumPob = 100     #Parametro JFabre 100
NumGen = 100     #Parametro JFabre 100

#Parametros del Optimizador basado en modelos
PobInc = 10      #ParametroJFabre 10
NumEva = 300      #(300) Deseable

#Descripcion de las intancias
#..................................................................................................................................................................................

#Esa es la infomracion de cada instancia para el problema de la mochila
#                Nombre     Valor
Instancias =  {
                'KP1.csv' : 21065,
                "KP2.csv" : 11390,
                "KP3.csv" : 15057,
                "KP4.csv" : 12432,
                "KP5.csv" : 13484,
                "KP6.csv" : 11557,
                "KP7.csv" : 15291,
                "KP8.csv" : 39811,
                "KP9.csv" : 24622,
                "KP10.csv": 29545,
                "KP11.csv": 24195,
                "KP12.csv": 29131,
                "KP13.csv": 22245,
                "KP14.csv": 33211,
                "KP15.csv": 80457,
                "KP16.csv": 52666,
                "KP17.csv": 59989,
                "KP18.csv": 49447,
                "KP19.csv": 61828,
                "KP20.csv": 45689,
                "KP21.csv": 71284
}


#Abro el archivo con las caracteristicas del problema a resolver
Archivo = NumInst
Objetos = np.asarray(pd.read_csv(RutInst+Archivo))
NumDim  = len(Objetos)
Dominio = [0,1]


#Creo el objeto que realiza la optimizacion basa en modelos 
#....................................................................................................................

#                              PobInc ,NumEva ,Semilla   
Pro1 = SMBO.Subset_MBO(TypeVar,PobInc ,NumEva ,   Seed)
    
#         Datos,  Valor 
Pro1.Inicializacion(Objetos, Instancias[NumInst],Ensambles[ModeloType])         

Pro1.Opimizacion_MBO()
#.....................................................................................................................
#Guardo la informacion de la experimentacion



#Importantes
if(GenArchivos == "Si"):
	#print("Se guarara el archivo")
	Info = pd.DataFrame(Pro1.Registro)
	Info.to_csv(Ruta+str(ModeloType)+"_Inst-"+NumInst+"_Seed-"+str(Seed)+".csv")

#Repositorios
Repo = []
for x in range(len(Pro1.Input)):
    Fila = []
    for y in range(len(Pro1.Input[0])):
        Fila.append(Pro1.Input[x][y])
    Fila.append(Pro1.OutPut[x])
    Repo.append(Fila)
      
if(GenArchivos == "Si"):
    Repo = pd.DataFrame(Repo)    
    Repo.to_csv(Ruta+"Repositorio_"+str(ModeloType)+"_Inst-"+NumInst+"_Seed-"+str(Seed)+".csv")
#print(pd.DataFrame(Pro1.Registro))
