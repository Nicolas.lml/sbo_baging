
'''
Este codigo construye y entrena un modelo de Abrol de decicion

'''

from smt.surrogate_models import RBF
from sklearn.tree import DecisionTreeRegressor
import numpy as np
import GS_Model


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_RBF(GS_Model.GridSearchCV):

    #Desde aqui son los metodos que se tiene que modificar para cada modelo
    def Inicializacion(self):

        self.Param = {
            'd0' : [1, 3, 5, 7, 9, 11]
            }
        
    #En este metodo construye el modelo con la combinacion A y se entrena
    def BuiltModel(self,X,Y,X_Test):

        self.Input = np.double(self.Input)
        self.Output = np.reshape(self.Output,(len(self.Output),1))  
        X_Test = np.double(X_Test)
        
        self.Model = RBF(
                d0=self.CombiA['d0'],
                print_global=False
                )        
        
        self.Model.set_training_values(self.Input, self.Output)
        self.Model.train()

        return self.Model.predict_values(X_Test)

    def Evaluacion(self,Individuo):
        #Aqui se calcula el fitness de combinacion. 
        Individuo = np.double(np.asarray([Individuo]))   
        Exit = self.Model.predict_values(Individuo)

        return Exit[0]
       
