

'''
Este algoritmo contiene una clase que optimiza un problema usando un algoritmo genetico planteado como problema continuo o discreto 
'''


'''
    Este codigo realiza el ensamble usanto un boosting
'''

import numpy as np
from timeit import default_timer as timer
import Modelo_DTree 
import Modelo_Kriging
import Modelo_SVM
import Modelo_KNN
import Modelo_RandomF
import Modelo_AdaBoost
import Modelo_RBF



class Ensamble_AG():
   
    #Metodo constructor con las variables que se utilizaran
    def __init__(self,Type,NumPob,NumGen,NumDim,Dominio,Semilla,TypeMod,Input,Output):
        
        #Tipo de problema de optmizacion
        self.Type = Type            #Optimizacion ("Ord")Ordinal/("Con")Continua,("Cat")Categorico 
        
        #Referente al algoritmo genetico
        self.NumPob = NumPob        #Numero de elementos para la poblacion
        self.NumGen = NumGen-1      #Numero de generaciones del algoritmo genetico
        self.Dominio = Dominio      #Rango de valores que puede tener Numero de valores que puede tener cada dimencion en el algoritmo genetico
        self.NumDim = NumDim        #Numero de dimenciones del problema
        self.BestInd = None         #En este atributo se guarda el mejor individuo encontrado
        self.BestFit = None         #En este atributo se guarda el fitness de mejor individuo encontrado
        self.Registro = []          #En este registro se guarda el progreso del fitness para la optimizacion

        #Aqui se declara la semilla para inicializar 
        self.Semilla = Semilla
        np.random.seed(Semilla)     #Semilla con la que trabajara el generador de numeros aleatoreos

        #Referente a los modelos
        self.Modelos = []
        self.TypeMod = TypeMod
        self.Input = Input
        self.Output = Output
        self.Predicciones = np.zeros((2*self.NumPob,len(self.TypeMod)))
        self.Jerarquias   = np.zeros((2*self.NumPob,len(self.TypeMod)))
        #Aqui se alojaran los cromosomas de la poblacion y sus Fitness
        self.Poblacion = np.zeros((2*self.NumPob,self.NumDim))   #En este arreglo que colocaran los cromosomas para cada individuo de la poblacion
        self.Fitness   = np.zeros(2*self.NumPob)                   #En este arreglo se guardaran el desempeño de cada algoritmo
        self.NewPobla  = np.zeros((self.NumPob,self.NumDim))
        self.NewFit    = np.zeros(self.NumPob)
        
        #Aqui se tienen los indices
        self.IndPadres = np.zeros(self.NumPob).astype(int)
        
        #Estos son los elementos auxiliares
        self.Limpios = 0                        #Esta indica los el numero de genotipos que no son relleno (Aqullos individuso que no son mutaciones de los principales para rellenar la poblacion)
        

    #Se uitiliza una cruza uniforme
    def Cruza(self,IndA,IndB):
        
        Ind1 = np.copy(IndA)
        Ind2 = np.copy(IndA)
        
        #La probabilidad de cruza es del 90%
        if(np.random.rand()<0.9):
            for c in range(len(IndA)):
                if(np.random.rand()>0.5):
                    Ind1[c] = IndB[c]
                else:
                    Ind2[c] = IndB[c]
        else:
            Ind2 = np.copy(IndB)
        
        return Ind1,Ind2

    #Esta funcion realiza la mutacion
    def Mutacion(self,Ind3,Iter):
        
        IndC = np.copy(Ind3)
        ProbM = 1.0/len(Ind3)    #Probabilidad de mutaicion

        #Para el caso continuo se utiliza una mutacion no uniforme (Apuntes de Coello)
        if(self.Type == "Con"):   
            
            for d in range(len(Ind3)):
                if(np.random.rand()<ProbM):
                    if(np.random.rand()<0.5):
                        aux = (self.Dominio[1]-IndC[d])*(1- (0.5**   ((1-Iter/self.NumGen)**5)))
                        IndC[d] += aux
                    else:
                        aux = (IndC[d]-self.Dominio[0])*(1- (0.5**   ((1-Iter/self.NumGen)**5)))
                        IndC[d] -= aux
                return IndC        
        
        #Para el caso discreto se elijen aleatoreamente los alelos y se mutan aleatereamente con un valor del dominio
        if(self.Type == "Dis"):
            
            for d in range(len(Ind3)):
                if(np.random.rand()<ProbM):
                    Control = IndC[d]
                    while(Control == IndC[d]):
                        IndC[d] = np.random.randint(self.Dominio[0],self.Dominio[1]+1)
                
            return IndC 

    
    #Esta funcion selecciona a los padres de la siguiente generacion
    def Seleccion(self):
        
        #Barajeo a los individuos para el torneo binario
        Baraja1 = np.random.permutation(self.NumPob)
        Baraja2 = np.random.permutation(self.NumPob)
        
        #Selecciono a los padres que se cruzarán 
        x = 0   #Posiciones
        for k in range(0,self.NumPob-1,2):
            
            if(self.Fitness[Baraja1[k]] < self.Fitness[Baraja1[k+1]]): self.IndPadres[x] = Baraja1[k]
            if(self.Fitness[Baraja1[k]] > self.Fitness[Baraja1[k+1]]): self.IndPadres[x] = Baraja1[k+1]
            if(self.Fitness[Baraja1[k]] == self.Fitness[Baraja1[k+1]]):
                if(np.random.rand()<0.5): self.IndPadres[x] = Baraja1[k]
                else: self.IndPadres[x] = Baraja1[k+1]
 
            if(self.Fitness[Baraja2[k]] < self.Fitness[Baraja2[k+1]]): self.IndPadres[x+int(self.NumPob/2)] = Baraja2[k]
            if(self.Fitness[Baraja2[k]] > self.Fitness[Baraja2[k+1]]): self.IndPadres[x+int(self.NumPob/2)] = Baraja2[k+1]
            if(self.Fitness[Baraja2[k]] == self.Fitness[Baraja2[k+1]]): 
                if(np.random.rand()<0.5): self.IndPadres[x+int(self.NumPob/2)] = Baraja2[k]
                else: self.IndPadres[x+int(self.NumPob/2)] = Baraja2[k+1]
            
            x = x+1
        
        #Para el caso en el que la poblacion no sea un numero par 
        if((self.NumPob % 2)!=0):
            if(self.Fitness[Baraja2[-1]] < self.Fitness[Baraja1[-1]]): self.IndPadres[-1] = Baraja2[-1]
            else: self.IndPadres[-1] = Baraja1[-1]
            self.Hijos[-1] = np.copy(self.Poblacion[self.IndPadres[-1]])
            
    #Esta funcion pone a competir a la poblacion con los hijos generados para identificar a los que pasan
    def Supervivencia(self,t):
        
        #Identifico una referencia 
        Ref = max(self.Fitness)

        
        #Selecciono al mejor individuo y verifico que no se repita
        Complete = False
        Todos = 0
        Conta = 0
        #Mientras no complete a la nueva poblacion
        #Y mientras todavia tenga individuos disponible
        while(Complete == False and Todos < 2*self.NumPob):  #Mientras, no se complete la nueva generacion y no se me acaben los individ

            if(Conta == 0): #Si es el primer elemento que se agrege
                BestH = np.where(self.Fitness==min(self.Fitness))[0][0]
                self.NewPobla[Conta] = np.copy(self.Poblacion[BestH])
                self.NewFit[Conta] = self.Fitness[BestH]
                self.Fitness[BestH] = Ref  
                Conta = Conta+1
                
            else:#Si no es el primero, reviso que no sea un genotipo igual al anterior
                BestH = np.where(self.Fitness==min(self.Fitness))[0][0]
                if(np.sum(np.power(self.Poblacion[BestH]-self.NewPobla[Conta-1],2)) != 0 ):
                    self.NewPobla[Conta] = np.copy(self.Poblacion[BestH])
                    self.NewFit[Conta] = self.Fitness[BestH]
                    self.Fitness[BestH] = Ref  
                    Conta = Conta+1
                else:
                    self.Fitness[BestH] = Ref 

            Todos = Todos+1            
            if(Conta == self.NumPob): 
                Complete = True
                
        #En el caso de que falten agregarse individuos
        aux = 0
        while(Conta+aux < self.NumPob):
            self.NewPobla[Conta+aux] = self.Mutacion(self.NewPobla[aux],self.NumGen)
            self.NewFit[Conta+aux] = self.Evaluacion(self.NewPobla[Conta+aux])
            aux = aux+1
        
        #Ahora la nueva poblacion es la poblacion actual
        for x in range(self.NumPob):
            self.Poblacion[x] = np.copy(self.NewPobla[x])
            self.Fitness[x] = self.NewFit[x] 
        
        self.Limpios = Conta 
        #print("De los hijos y padres: ",Conta,"Numero de H y P revisados", Todos) 


    #Este metodo utiliza un algortimo genetico para optimizar la funcion 
    def Optimizacion(self):
        
        start = timer()
        
        #Se crean y entrenan los modelos que se utilizaran 
        self.GetModels()
        
        #Se genera la poblacion deacuerdo al tipo de dato y se calcula su aptitud
        self.GenPobla()
        self.Prediccion(0,2*self.NumPob)      #Predigo los fitness con los modelos
        self.Aver()                           #Calculo el modelo del esamble 
        
        #print("Estos son los Fitneess",self.Fitness)
        #Inician las generaciones
        for gen in range(self.NumGen):

            #SELECCION: Selecciono a los padres con la poblacion actual
            self.Seleccion()

            #CRUZA: Cruzo a los padres seleccionados para obtener a sus hijos
            for i in range(0,self.NumPob-1,2):
                self.Poblacion[self.NumPob+i],self.Poblacion[self.NumPob+i+1]= self.Cruza(self.Poblacion[self.IndPadres[i]],self.Poblacion[self.IndPadres[i+1]])                   
                
            #MUTACION a los nuevos individos generados
            for i in range(self.NumPob):
                self.Poblacion[i+self.NumPob] = self.Mutacion(self.Poblacion[i+self.NumPob],i)
            
            #APTITUD: Se mide el fitness de cada individuo
            self.Prediccion(self.NumPob,2*self.NumPob)      #Predigo los fitness con los modelos
            self.Aver()                                #Calculo el modelo del esamble                 

            #Supervivencia: los hijos compiten con la generacino pasada para generar la nueva poblacion  
            self.Supervivencia(gen)


            #Aqui se guarda el registro del comportamiento de optimizacion
            self.Registro.append(self.Fitness[0])
        
        self.BestFit = self.Evaluacion(self.Poblacion[0])       
        self.BestInd = np.copy(self.Poblacion[0])

        #Se guarda el tiempo total de ejecucion
        end = timer()        
        self.Registro.append(end-start)
            

    #Este metodo genera la poblacion inicial aleatorea
    def GenPobla(self):
        
        if(self.Type == "Con"):
            self.Poblacion = np.random.rand(self.NumPob,self.NumDim) * (self.Dominio[1]-self.Dominio[0]) + self.Dominio[0] #Inicializo una poblacion aleatoria

        if(self.Type == "Dis"):
            self.Poblacion = np.random.randint(self.Dominio[0],self.Dominio[1]+1,(2*self.NumPob,self.NumDim))    #Inicializo una poblacion aleatoria
            

    #El metodo de inicializacion, agrega o define las variables faltantes
    def Inicializacion(self):
        pass
    
    #En este metodo se pograma la funcion objetivo con el que se les da la aptitud a los individuos
    def Evaluacion(self,Individuo):
        pass

    #Este metodo construye los modelos que se utilizaran
    def GetModels(self):
        
        for i in range(len(self.TypeMod)):
            if(self.TypeMod[i] == "SVM")      : self.Modelos.append(Modelo_DTree.GS_DTree(self.Input,self.Output,self.Semilla))
            if(self.TypeMod[i] == "DTree")    : self.Modelos.append(Modelo_SVM.GS_SVM(self.Input,self.Output,self.Semilla))
            if(self.TypeMod[i] == "Kriging")  : self.Modelos.append(Modelo_Kriging.GS_Kriging(self.Input,self.Output,self.Semilla))
            if(self.TypeMod[i] == "KNN")      : self.Modelos.append(Modelo_KNN.GS_KNN(self.Input, self.Output, self.Semilla)) 
            if(self.TypeMod[i] == "RandomF")  : self.Modelos.append(Modelo_RandomF.GS_RandomF(self.Input, self.Output, self.Semilla))
            if(self.TypeMod[i] == "AdaBoost") : self.Modelos.append(Modelo_AdaBoost.GS_AdaBoost(self.Input, self.Output, self.Semilla))
            if(self.TypeMod[i] == "RBF")      : self.Modelos.append(Modelo_RBF.GS_RBF(self.Input, self.Output, self.Semilla))

            #Construyo el modelo que se acaba de agregar 
            self.Modelos[i].Ejecucion()
            
    #Este metodo utiliza los modelos para evaluar a los individuos
    def Prediccion(self,Begin,End):
        
        for i in range(len(self.Modelos)):
            for j in range(Begin,End):
                self.Predicciones[j][i] =  self.Modelos[i].Evaluacion(self.Poblacion[j])
        
        #print("Estas son las predicciones")
        #print(self.Predicciones)
        
    #Este metodo obtiene las jerarquias de todos de la poblacion con todos los modelos
    def Jerar(self):
        
        for i in range(len(self.Modelos)):
            Sort = np.argsort(self.Predicciones[:,i])
            
            for j in range(2*self.NumPob):
                self.Jerarquias[Sort[j]][i] = j
        
        #print("estas son las jerarquias")
        #print(self.Jerarquias)
    
    #Este metodo calcula el fitness de la poblacion sumando todas jerarquias obtenidas
    def Aver(self):
        
        for i in range(2*self.NumPob):
            self.Fitness[i] = np.sum(self.Predicciones[i])
        #print("Estos son los fitness")
        #print(self.Fitness)        
  
    
    
'''        
        
#A continuacion se tiene un ejemplo de la manera en que se utiliza el algoritmo

#Se crea una clase con la funcio objetivo que se desea minimizar
class Esfera(Ensamble_AG):
    
    #Aqui se llena el metodo que contiene la funcion objetivo con la que se evaluan los individuos
    def Evaluacion(self,Individuo):
        
        aux = 0
        for i in range(len(Individuo)):
            aux += Individuo[i]**2
        
        return aux


np.random.seed(1)


Semilla = 1
NumGen = 10
NumPob = 10

NumDim = 10
Dominio = [0,10]

PobInc = 10

Input = np.random.randint(Dominio[0],Dominio[1],(PobInc,NumDim))
Output = np.sum(np.power(Input,2),axis=1)
TypeMod = ["DTree","SVM"]
#Se inicializa la clase
#               Type ,NumPob,NumGen,NumDim,Dominio,Semilla, Modelos,Input,Output
Prueba = Esfera("Dis",NumPob,NumGen,NumDim,Dominio,Semilla, TypeMod,Input,Output)
Prueba.Optimizacion()
print("El optimo es: ",Prueba.BestFit)
print(Prueba.Limpios)
print(Prueba.Fitness)
#print(Prueba.Poblacion)


'''