


'''
Este codigo construye y entrena un modelo de Abrol de decicion

'''


from sklearn.ensemble import RandomForestRegressor
import GS_Model


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_RandomF(GS_Model.GridSearchCV):

    #Desde aqui son los metodos que se tiene que modificar para cada modelo
    def Inicializacion(self):

        #Parametros que evaluare en la busqueda exhaustiva
        self. Param = {
            'n_estimators' : [100],
            'max_depth' : [10,20,None],
            'max_features' : ["sqrt", None]
        } 

    #En este metodo construye el modelo con la combinacion A y se entrena
    def BuiltModel(self,X,Y,X_Test):
        
        self.Model = RandomForestRegressor(
                n_estimators = self.CombiA['n_estimators'],
                max_depth = self.CombiA['max_depth'],
                max_features = self.CombiA['max_features'] 
                )
        
        self.Model.fit(X,Y)
        return self.Model.predict(X_Test)

    def Evaluacion(self,Individuo):
        #Aqui se calcula el fitness de combinacion. 
        Exit = self.Model.predict([Individuo])
        return Exit[0]
       

'''

Semilla = 1
NumGen = 10
NumPob = 10

NumDim = 10
Dominio = [0,10]

PobInc = 20

import numpy as np
def Esfera(Input):
    
    Out = np.sum(np.power(Input,2),axis = 1)
    #Out = np.reshape(Out,(len(Input),1))
    return Out

#Creo el conjunto de prubea
Input = np.random.randint(Dominio[0],Dominio[1],(PobInc,NumDim))
Output = np.sum(np.power(Input,2),axis=1)


Prue = GS_DTree(Input,Output,1)
Prue.Ejecucion()
print(Prue.CombiA)
print(Prue.ECM)
print(Prue.Evaluacion([0,0,0]))
'''

