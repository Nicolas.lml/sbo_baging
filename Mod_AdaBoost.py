'''
    El el codigo contiene una clase que uitiliza un algoritmo genetico para optmizar un modelo basado en
    AdaBoost  
'''


'''
    Jefe Fabre

        Comentarios 
            Detalles de random forest: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostRegressor.html
                
        Hiperparametros
            'n_estimators' : [10,30,50,80],
            'loss' : ['linear','square','exponential']
        
'''


from sklearn.ensemble import AdaBoostRegressor
import GridSearch
from sklearn.model_selection import GridSearchCV
import AG 


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_AdaB(GridSearch.GridSearchCV):
    
    #En este metodo construye el modelo y retorna la prediccion asocidad usando la combinavion CombiA
    def BuiltModel(self,X,Y,X_Test):
        
        self.Model = AdaBoostRegressor(
                n_estimators = self.CombiA['n_estimators'],
                loss = self.CombiA['loss']
                )
        
        #Entreno el modelo
        self.Model.fit(X,Y)
        
        return self.Model.predict(X_Test)


#Creo un objeto que realize la optimizacion interna utilizando un algoritmo genetico
class AdaBoost_AG(AG.AG):

    #Este metodo contiene la informacion de entrenamiento con la que se crea el modelo
    def Inicializacion(self,Input,Output,NumDim):
        
        #Inicializo el modelo que utilizare
        self.Modelo = AdaBoostRegressor(random_state=0, n_estimators=100)
        
        #Parametros que evaluare en la busqueda exhaustiva
        Param = {
            'n_estimators' : [50,100],
            'loss' : ['linear','square','exponential']
            #'n_estimator' : 
        } 
        
        NumP = 5 #Numero de pliegues para la validacion cruzada        
        
        #Inicializo el GridSearch  
        GS = GS_AdaB(Param,NumP,Input,Output,self.Semilla)
        GS.Ejecucion()        
        self.Modelo = GS.Model

        self.BestParam = GS.CombiA
        self.BestECM   = GS.ECM
        self.BestPend  = GS.Pendiente
        

        #print("ECM: ",GS.ECM, " Pend: ",GS.Pendiente," Parametros: ",GS.CombiA)
        
    #Este es el metodo para evaluar un candidato con el modelo que se este trabajando
    def Evaluacion(self,Individuo):
        
        #Aqui se calcula el fitness de combinacion. 
        Exit = self.Modelo.predict([Individuo])
        return Exit[0]
    

'''
#################################################################################################

import pandas as pd

#Abro el archivo con las caracteristicas del problema a resolver
Archivo = "KP1.csv"
Objetos = np.asarray(pd.read_csv(Archivo))

Exper = []

for i in range(1):
    
    #Creo el modelo con el se trabajara 
    #                     NumPob ,NumGen  ,NumDim      ,Dominio ,Semilla
    Modelo = Knap_RedN_AG(200    ,100     ,len(Objetos),[0,1]   ,i)
    
    #Creo el objeto que realiza la la optimizacion basa en modelos 
    #               PobInc,NumEva,Semilla , Modelo   
    Pro1 = KMBO.MBO_Knap(20    ,25    , i      , Modelo)
    #               Objetos,Capacidad
    Pro1.Inicio_MBO(Objetos,970)
    Pro1.Opimizacion_MBO()
    Pro1.Registro.append(Pro1.CalcPeso(Pro1.SolInd))
    Exper.append(Pro1.Registro)
    #print(Exper[-1])

#Guardo la informacion en un archivo del tipo csv
Info = pd.DataFrame(Exper)
#Info.to_csv("MBO_K6_200"+Archivo+".csv")
'''