
'''
Este codigo construye y entrena un modelo de Abrol de decicion

'''


from sklearn import linear_model
import numpy as np
from smt.surrogate_models import KRG


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_Kriging():
    
    def __init__(self,Input,Output,Semilla):
        self.Input = Input
        self.Output = Output
        self.Semilla = Semilla
    

        #Correcciones para el modelo
        Input = np.double(Input)
        Output = np.reshape(Output,(len(Output),1))
        
        #Creo y entreno el modelo
        self.Modelo = KRG(print_global=False)
        self.Modelo.set_training_values(Input, Output)
        self.Modelo.train()        
        
        #Medicion de parametros del modelo
        Predict = self.Modelo.predict_values(Input)
        Pen = linear_model.LinearRegression()
        Pen = Pen.fit(Predict.reshape(-1, 1),Output)        
        
        self.BestECM   = np.sum(np.sqrt(np.power(Predict-Output,2)))
        self.BestParam = "--"        
        self.BestPend  = Pen.coef_[0][0]


    def Evaluacion(self,Individuo):
        #Aqui se calcula el fitness de combinacion.
        Individuo = np.double(np.asarray([Individuo]))      
        Exit = self.Modelo.predict_values(Individuo)
        return Exit[0]
      
    def Ejecucion(self):
        pass

'''
def Esfera(Input):
    
    Out = np.sum(np.power(Input,2),axis = 1)
    #Out = np.reshape(Out,(len(Input),1))
    return Out

#Creo el conjunto de prubea
Input  = np.random.randint(0,10,(50,3))     #Genero los valores de entrada
Output = Esfera(Input)           #Genero los puntos de salida


Prue = GS_Kriging(Input,Output,1)


print(Prue.BestECM)
print(Prue.Evaluacion([0,0,0]))




#################################################################################################

import pandas as pd

#Abro el archivo con las caracteristicas del problema a resolver
Archivo = "KP1.csv"
Objetos = np.asarray(pd.read_csv(Archivo))

Exper = []

for i in range(1):
    
    #Creo el modelo con el se trabajara 
    #                     NumPob ,NumGen  ,NumDim      ,Dominio ,Semilla
    Modelo = Knap_RedN_AG(200    ,100     ,len(Objetos),[0,1]   ,i)
    
    #Creo el objeto que realiza la la optimizacion basa en modelos 
    #               PobInc,NumEva,Semilla , Modelo   
    Pro1 = KMBO.MBO_Knap(20    ,25    , i      , Modelo)
    #               Objetos,Capacidad
    Pro1.Inicio_MBO(Objetos,970)
    Pro1.Opimizacion_MBO()
    Pro1.Registro.append(Pro1.CalcPeso(Pro1.SolInd))
    Exper.append(Pro1.Registro)
    #print(Exper[-1])

#Guardo la informacion en un archivo del tipo csv
Info = pd.DataFrame(Exper)
#Info.to_csv("MBO_K6_200"+Archivo+".csv")
'''