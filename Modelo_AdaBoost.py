

'''
Este codigo construye y entrena un modelo de Abrol de decicion

'''



from sklearn.ensemble import AdaBoostRegressor
import GS_Model


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_AdaBoost(GS_Model.GridSearchCV):

    #Desde aqui son los metodos que se tiene que modificar para cada modelo
    def Inicializacion(self):

        #Parametros que evaluare en la busqueda exhaustiva
        self.Param = {
            'n_estimators' : [50,100],
            'loss' : ['linear','square','exponential']
            #'n_estimator' : 
        } 
        

    #En este metodo construye el modelo con la combinacion A y se entrena
    def BuiltModel(self,X,Y,X_Test):
        
        self.Model = AdaBoostRegressor(
                n_estimators = self.CombiA['n_estimators'],
                loss = self.CombiA['loss']
                )
        
        self.Model.fit(X,Y)
        return self.Model.predict(X_Test)

    def Evaluacion(self,Individuo):
        #Aqui se calcula el fitness de combinacion. 
        Exit = self.Model.predict([Individuo])
        return Exit[0]