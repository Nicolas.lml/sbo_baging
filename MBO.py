'''
El el codigo contiene una clase abstacta para realizar la optimizacion basada en modelos de una funcion utilizando variables discretas 

'''

import numpy as np
import Ensamble
from timeit import default_timer as timer

#Esta clase contiene el procedimiento para realizar una optimizacion basada en modelos para el problema de la mochila, utilizando un objeto que contenga un optimizador interno  y un modelo
class MBO():
    
    #Este es el metodo constructor del objeto
    #El modelo debe ser una clase que herede de 
    def __init__(self,Type,PobInc,NumEva,Semilla):

        #Referente al MBO
        self.Type   = Type                   #Optimizacion ("Dis")Discreta/("Con")Continua 
        self.PobInc = PobInc                 #Poblacion inicial para construir el Metamodelos        
        self.NumEva = NumEva - PobInc        #Numero de evaluaciones restantes en la funcion costosa
        self.Input  = []                     #Inptus de entrenamiento del modelo
        self.OutPut = []                     #Outputs de entrenamiento del modelo
        self.NumDim = None                   #numero de Dimenciones que tiene el problema
        self.Dominio = None                  #Numero de variables que puede tener cada dimencion
        self.SolFit = None                   #Fitness de la mejor solucion                    
        self.SolPes = None                   #Peso de la mejor solucion
        self.SolInd = None                   #Cromosoma de la mejor solucion
        self.Registro = []                   #Aqui se guarda la mejor solucion a travez de las iteraciones 
        self.TiempoTot = 0                   #Aqui se guarda el tiempo total consumido 
        self.InfoIter = []
        
        #Otros
        self.Semilla = Semilla

    #Este es el cuerpo del porceso de optimizacion basado en modelos
    def Opimizacion_MBO(self):
        
        #Empiezo la medicion del tiempo
        start = timer()
        
        #Para la cantidad de evaluaciones requeridas
        for gen in range(self.NumEva):
            
            print(gen)            
            #Primero se crea el objeto con el modelo 
            self.GetModel()
            
            self.Semilla=gen
            
            #Agrego el nuevo individuo al Input
            newInd = self.Modelo.BestInd          
            self.Input = np.append(self.Input,[np.copy(newInd)],axis=0)
            self.OutPut.append(self.Evaluacion_Real(newInd))
            
            #Identifico el Optimo
            self.IdentBest()
            
            print("NewCromosoma ",self.Modelo.BestInd)
            print("Mejor Fit: ",self.SolFit,"   New Fit  : ",self.OutPut[-1],"    Calculado  :",self.Modelo.BestFit)
            print("\n")  
            
            #Organizo la informacion que guardare por iteracion 
            self.InfoIter = []
            end = timer()
            self.InfoIter.append(end - start)             #Agrego el tiempo en el tiempo total en el que se llego a esta solucion
            self.InfoIter.append(self.PosAux)            #Posicion de la mejor solucion alcanzada
            self.InfoIter.append(self.SolFit)             #Mejor fitnees alcanzado
            self.InfoIter.append(self.OutPut[-1])         #Fitness calculado en la iteracion
            self.InfoIter.append(self.Modelo.BestFit)     #Estimacion realizada                
            #self.InfoIter.append(self.Modelo.BestParam)   #Mejores parametros encontrados
            #self.InfoIter.append(self.Modelo.BestECM)     #ECM de la mejor configuracion
            #self.InfoIter.append(self.Modelo.BestPend)        ##Pendiente del mejor modelo
  
            
            #Guardo la informacion de la iteracion
            self.Registro.append(self.InfoIter)
            
    #Este metodo especifica el tipo de objeto y modelo con el que se trabajará
    def GetModel(self):
        
        self.Modelo = Ensamble.Ensamble_AG("Dis", self.NumPob, self.NumGen, self.NumDim, self.Dominio, self.Semilla, self.Modelos, self.Input, self.OutPut)
        self.Modelo.Optimizacion()  
        
        #Se revisa que la nueva solucion propuesta no se repita con alguna del repositorio
        Nuevo = False     #Bandera para identificar al nuevo elemento
        self.cnt = 0           #Contador
        #print("Genotipos limpios :",self.Modelo.Limpios)

        while(Nuevo == False and self.cnt < self.Modelo.Limpios): 
            
            Found = False    #Bandera de localizacion            
            #Reviso todo el repositorio
            for i in range(len(self.Input)):
                
                #Se revisa que tengan el mismo genotipo
                if(np.sum(np.power(self.Modelo.Poblacion[self.cnt]-self.Input[i],2))==0):
                    #if(cnt == 2): print("Resta: ",self.Modelo.Poblacion[cnt]-self.Input[i])
                    Found = True
            
            if(Found == False):
                self.Modelo.BestFit = self.Modelo.Fitness[self.cnt]
                self.Modelo.BestInd = np.copy(self.Modelo.Poblacion[self.cnt])   
                #print("El número de poblacion con un elemento nuevo es:",cnt)                     
                Nuevo = True                   
            
            #Reviso si todavia se tienen individuos en el AG, en caso contrario, muto la mejor solucion
            if(self.cnt == self.Modelo.NumPob-1): 
                #print(self.Modelo.BestInd,self.Modelo.BestFit)    
                self.Modelo.BestInd = self.Modelo.Mutacion(self.Modelo.BestInd,1)
                self.Modelo.BestFit = self.Modelo.Evaluacion(self.Modelo.BestInd)
                #print("Salio acabandose a los individuos:")
                Nuevo = True
                
            self.cnt = self.cnt+1 
                   
    
    #Este metodo identifica la mejor solucion hasta el momento 
    def IdentBest(self):
        
        self.posBest = 0
        for i in range(len(self.OutPut)):
            if(self.OutPut[i]<self.OutPut[self.posBest]): self.posBest = i
                
        if(self.posBest < self.PobInc): self.PosAux = 0
        else: self.PosAux = self.posBest - self.PobInc 
        self.SolFit = self.OutPut[self.posBest]
        self.SolInd = self.Input[self.posBest]
        
    #Esta funcion contiene la funcion real de evaluacion del problema
    def Evaluacion_Real(self,Individuo):    
        pass
    
    #En este metodo se inicializan la informacion faltante del problema de la mochila
    def Inicializacion(self,NumDim,Dominio,Modelos):
        
        self.NumDim = NumDim
        self.Dominio = Dominio
        self.Modelos = Modelos
        self.NumPob = 100
        self.NumGen = 100
        self.Modelos = Modelos
        
        
        #Se crea una primera poblacion aleatoria con la que se entrenara el modelo
        np.random.seed(self.Semilla)
        
        if(self.Type == "Dis"):
            self.Input = np.random.randint(self.Dominio[0],self.Dominio[1]+1,(self.PobInc,self.NumDim))

        if(self.Type == "Con"):
            self.Input = np.random.rand(self.PobInc,self.NumDim) * (self.Dominio[1]-self.Dominio[0]) + self.Dominio[0] #Inicializo una poblacion aleatoria
        
        for i in range(self.PobInc):
            self.OutPut.append(self.Evaluacion_Real(self.Input[i]))

    
    #Se crea el ensamble que se utilizara 
            
        

    