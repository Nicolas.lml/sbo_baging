

'''
Este codigo resuelve el problema de la mochila utilizado el algoritmo EGO (Kriging)


'''


import numpy as np
import pandas as pd
from smt.applications import EGO
from smt.surrogate_models import KRG
from smt.sampling_methods import LHS
from smt.applications.mixed_integer import (
    FLOAT,
    INT,
    ENUM,
    MixedIntegerSamplingMethod,
)


#Esta clase resuelve el problema de la mochila utilizando el algoritmo EGO
class EGO_Knap():
    
    def __init__(self,PobInc ,NumEva    ,Semilla, Objetos, Capacidad):
        
        #Parametros del problema
        self.PobInc = PobInc
        self.NumEva = NumEva
        self.Semilla = Semilla
        self.Objetos = Objetos
        self.Capacidad = Capacidad
        self.NumDim = len(self.Objetos)
        self.Dominio = [0,1]
        
        #Creacion del modelo y sus variables
        self.Xtypes    = [(ENUM,self.Dominio[1]+1) for i in range(self.NumDim)]     #Por cada dimencion se define el TIPO de variable y La cantidad de variables que posee la dimencion
        self.Variables = [str(i) for i in range(self.Dominio[1]+1)]                 #Para todas las variables (dimenciones) los datos son categoricos, por lo que escribo el nombre de la categoria (por facilidad genero cada nombre con un string de una enumeracion)
        self.Xlimits   = np.array([self.Variables for i in range(self.NumDim)])     #A cada Dimencion le asigno sus respectivas categorias (Variables, ya que todas las categorias tiene la misma dimencion)
        #EI : expected improvment   SBO : Mejor solucion
        self.Criterion ="SBO"                                                        #Criterio con base en el que se realiza la busqueda.    
        self.Modelo    = KRG(print_global=False)                                    #Creo la clase con el modelo que utilizare
        
        #R es un factor que se calcula para las penalizaciones
        self.R=0
        for i in range(len(self.Objetos)):
            for j in range(len(self.Objetos)):
                if(self.R < (self.Objetos[i][0] / self.Objetos[i][1])): self.R = self.Objetos[i][0] / self.Objetos[i][1]
                
        #Se inicializa la semilla y se crean los valores inicales de muestreo aletaoremente
        np.random.seed(self.Semilla)
        self.Input = np.random.randint(self.Dominio[0],self.Dominio[1]+1,(self.PobInc,self.NumDim))
        self.Output = self.Evaluacion(self.Input)
        
        #Creo el optimizador que se utilizara 
        ego = EGO(
            n_iter=self.NumEva-self.PobInc,
            criterion=self.Criterion,
            xdoe=self.Input,
            ydoe=self.Output,
            xtypes=self.Xtypes,
            xlimits=self.Xlimits,
            surrogate=self.Modelo,
        )
        
        x_opt, y_opt, _, _, y_data = ego.optimize(fun=self.Evaluacion)
        #print("Minimum in x={} with f(x)={:.1f}".format(x_opt, float(y_opt)))


    #Esta funcion evalua a la poblacion o individuo (Mide el peso asociado a cada individuo)    
    def Evaluacion(self,Individuos):
        
        Output = np.ones(len(Individuos))
        
        
        for i in range(len(Individuos)):
            #Aqui se calcula el fitness de combinacion. 
            valor = 0
            peso = 0            
            for j in range(self.NumDim):
                if(Individuos[i][j]==1):
                    peso += self.Objetos[i][1]
                    valor += self.Objetos[i][0]
            #PENALIZACION 
            if(peso > self.Capacidad):
                valor -= ((peso - self.Capacidad )* self.R)**2
                
            Output[i] = -valor
        
        #print(Individuos)
        #print(Output)
        return Output                  
        

#########################################################################################
'''
#Esa es la infomracion de cada instancia para el problema de la mochila
#          Nombre        Capacidad  Valor
Instancias = [["KP1.csv"    ,970      ,1428],
              ["KP2.csv"    ,970      ,970],
              ["KP3.csv"    ,970      ,1989],
              ["KP4.csv"    ,997      ,2586],
              ["KP5.csv"    ,970      ,1005],
              ["KP6.csv"    ,970      ,3408]]

Carpeta = "./Instancias/"

#Parametros de la Experimentacion
GenArchivos = "No"
NumExp = 20     #Numero de experimentos a realizar
NumInst = 1    #Numero de instancias que se evaluaran


#Parametros del Optimizador basado en modelos
PobInc = 5
NumEva = 10

#Aqui empieza la experimentacion
for inst in range(NumInst):
    
    inst = 3
    #Abro el archivo con las caracteristicas del problema a resolver
    Archivo = Instancias[inst][0]
    Objetos = np.asarray(pd.read_csv(Carpeta+Archivo))
    Dominio = [0,1]
    NumDim = len(Objetos)
    print("\nAqui empezamos con el experimento ", inst,"\n")   

    #Aqui se llevara el registro de las experimentaciones obtenidas 
    Exper = []
    
    for i in range(NumExp):

        print("Numero de experimento: ",i)

        #Creo el objeto que realiza la la optimizacion basa en modelos 
        #               PobInc ,NumEva    ,Semilla, Objetos, Capacidad   
        Pro1 = EGO_Knap(PobInc ,NumEva    , i     , Objetos, Instancias[inst][1])


    
     
        Pro1.Inicializacion(Objetos,Instancias[inst][1])
        Pro1.Opimizacion_MBO()
        
        Exper.append(Pro1.Registro)
    
    #Guardo la informacion en un archivo del tipo csv
    if(GenArchivos == "Si"):
        Info = pd.DataFrame(Exper)
        Info.to_csv(".\Resultados\RedNeu_.csv")
'''

