import os,sys,shutil

################################################################################
# Ejecucion local o en Cluster?
################################################################################

#ejecutar = "LOCAL"
ejecutar = "CLUSTER"

################################################################################
# Rutas
################################################################################

# Directorio actual (donde se encuentra el ejecutable)
WORKDIR = os.getcwd()

# Directorio donde se crearan los scripts para ejecucion en el cluster
SCRIPTDIR = "%s/QSUBSCRIPTS/" % WORKDIR

# Directorio donde estaran los archivos de salida/error estandar (necesario en cluster)
STDOUTDIR = "%s/STDOUTERR/" % WORKDIR

# Directorio donde se colocaran los resultados de los experimentos
RESULTS_PATH = WORKDIR + "/RESULTADOS/"

# Directorio de los datasets/problemas
INSTANCIAS  = WORKDIR + "/Instancias/"

################################################################################
# Parametros
################################################################################

RUNS = 20

problemas = [ 
    "KP1.csv",
    "KP2.csv",
    "KP3.csv",
    "KP4.csv",
    "KP5.csv",
    "KP6.csv",
    "KP7.csv",
    #"KP1.csv",
    #"KP8.csv",
    #"KP9.csv",
    #"KP10.csv",
    #"KP11.csv",
    #"KP12.csv",
    #"KP13.csv",
    #"KP14.csv"
]

Ensambles = [
    #"Top1Sub",
    #"Top6Sub",
    "Top4Sub",
    "Top4Esp",
        ]

    

################################################################################
# Preparar scripts con ejecuciones
################################################################################

# Crear directorios para los scripts y archivos de salida estandar
shutil.rmtree(SCRIPTDIR, ignore_errors=True)
shutil.rmtree(STDOUTDIR, ignore_errors=True)
try: 
    os.makedirs(SCRIPTDIR)
    os.makedirs(STDOUTDIR)
except:
	pass

# Definir total de scripts ejecutables que se crearan (Numero de cores que se utilizaran)
NUM_SCRIPTS = 20
script_idx = 0 # Dejar siempre en 0

# Esta funcion va repartiendo ejecuciones entre los diferentes scripts
def agregar_ejecucion( texto_ejecutar ):

	global script_idx

	# Decidir en que script agregar ejecucion
	script_idx += 1
	if script_idx > NUM_SCRIPTS:
		script_idx = 1

	# Agregar ejecucion
	with open("%sSCRIPT_%d.sh" % (SCRIPTDIR, script_idx), "a") as fp:
		fp.write("%s\n" % texto_ejecutar)

# Esta funcion permite agregar a los scripts un comando extra para ir
# registrando el avance logrado en cada uno de ellos
def registrar_avance(problema, modelo):

	# Agregar reporte de avance en cada script
	for i in range(1,NUM_SCRIPTS+1):

		texto_avance = "Completado - %s - %s | $(date)" % (problema, modelo)

		with open("%sSCRIPT_%d.sh" % (SCRIPTDIR, i), "a") as fp:
			fp.write('echo "%s" >> %savance_SCRIPT_%d.txt\n' % (texto_avance, SCRIPTDIR, i))

################################################################################
# Crear todas las ejecuciones individuales
################################################################################


Save = "Si"
# Una ejecucion por cada combinacion de metamodelo, problema y repeticion
for problema in problemas:
    for Ens in Ensambles:
        ruta = RESULTS_PATH + "%s/%s/" % (problema, Ens)
        
        #Creo las carpetas en donde se guarda la informacion
        try: 
            os.makedirs(ruta)
        except:
            pass
        
        #Ejecuto todos los experimetnos con la instnacia y le modelo 	
        for repeticion in range(1,RUNS+1):
            # Crear comando para ejecutar
            texto_ejecutar = "python " + WORKDIR + "/Demonio.py %s %s %s %d %s %s" % (problema,Ens,Save,repeticion,ruta,INSTANCIAS)

			# Agregar ejecucion 
            agregar_ejecucion(texto_ejecutar)

		# Reportar avance
		#registrar_avance(problema, Ensambles[Ens])

################################################################################
# Ejecutar scripts
################################################################################

if ejecutar == "LOCAL":

	# Ejecutar cada uno de los scripts
	for i in range(1,NUM_SCRIPTS+1):

		print("Ejecutando %sSCRIPT_%d.sh" % (SCRIPTDIR, i))
		os.popen( "sh %sSCRIPT_%d.sh &" % (SCRIPTDIR, i) )

elif ejecutar == "CLUSTER":

	# Crear un script del cluster como wrapper para cada script original
	# Ejecutar cada uno de los scripts del cluster
	for i in range(1,NUM_SCRIPTS+1):

		# Nombre de los archivos
		script = "%sSCRIPT_%d.sh" % (SCRIPTDIR, i)
		cluster_script = "%scluster_script_%d.sh" % (SCRIPTDIR, i)

		# Preparar encabezado
		text = "#!/bin/bash\n"
		text += "#PBS -V\n"
		text += "#PBS -M nicolas.cortes@cinvestav.mx\n"
		text += "#PBS -m bea\n" 
		text += "#PBS -N META%d\n" % (i)
		text += "#PBS -q q4\n" 
		text += "#PBS -l nodes=1:ppn=1\n"
		text += "#PBS -o %s\n" % STDOUTDIR
		text += "#PBS -e %s\n" % STDOUTDIR
		text += "cd $PBS_O_WORKDIR\n\n"

		# Agregar comando que se ejecutara
		text += "sh %s\n\n" % script

		# Crear archivo de script
		fp = open(cluster_script, "w")
		fp.write(text)
		fp.close()

		# Lanzar trabajo a cluster
		print("Ejecutando %s" % cluster_script)
		os.popen( "qsub %s" % cluster_script)
