
'''
Este codigo construye y entrena un modelo de Abrol de decicion

'''

import GS_Model
from sklearn.svm import SVR


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_SVM(GS_Model.GridSearchCV):

    #Desde aqui son los metodos que se tiene que modificar para cada modelo
    def Inicializacion(self):

        self. Param = {
            'C' : [ 1, 100, 1000, ],
            'gamma' : [ 'scale','auto'],   #Mochila
            'kernel' : ['rbf']
        } 

    #En este metodo construye el modelo con la combinacion A y se entrena
    def BuiltModel(self,X,Y,X_Test):
        
        self.Model = SVR(
                C = self.CombiA['C'],
                gamma = self.CombiA['gamma'],
                kernel = self.CombiA['kernel']
                )
        
        #Entreno el modelo
        self.Model.fit(X,Y)
        
        return self.Model.predict(X_Test)

    def Evaluacion(self,Individuo):
        #Aqui se calcula el fitness de combinacion. 
        Exit = self.Model.predict([Individuo])
        return Exit[0]
       

'''

import numpy as np
def Esfera(Input):
    
    Out = np.sum(np.power(Input,2),axis = 1)
    #Out = np.reshape(Out,(len(Input),1))
    return Out

#Creo el conjunto de prubea
Input  = np.random.randint(0,10,(50,3))     #Genero los valores de entrada
Output = Esfera(Input)           #Genero los puntos de salida


Prue = GS_SVR(Input,Output,1)
Prue.Ejecucion()
print(Prue.CombiA)
print(Prue.ECM)
print(Prue.Evaluacion([0,0,0]))




#################################################################################################

import pandas as pd

#Abro el archivo con las caracteristicas del problema a resolver
Archivo = "KP1.csv"
Objetos = np.asarray(pd.read_csv(Archivo))

Exper = []

for i in range(1):
    
    #Creo el modelo con el se trabajara 
    #                     NumPob ,NumGen  ,NumDim      ,Dominio ,Semilla
    Modelo = Knap_RedN_AG(200    ,100     ,len(Objetos),[0,1]   ,i)
    
    #Creo el objeto que realiza la la optimizacion basa en modelos 
    #               PobInc,NumEva,Semilla , Modelo   
    Pro1 = KMBO.MBO_Knap(20    ,25    , i      , Modelo)
    #               Objetos,Capacidad
    Pro1.Inicio_MBO(Objetos,970)
    Pro1.Opimizacion_MBO()
    Pro1.Registro.append(Pro1.CalcPeso(Pro1.SolInd))
    Exper.append(Pro1.Registro)
    #print(Exper[-1])

#Guardo la informacion en un archivo del tipo csv
Info = pd.DataFrame(Exper)
#Info.to_csv("MBO_K6_200"+Archivo+".csv")
'''
