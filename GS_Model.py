'''
    Este codigo desarrolla la herramienta de busqueda exhaustiva usando cross validation
'''

import numpy as np
import itertools
from sklearn import linear_model


#Esta clase cotiene la Herramienta de Validacion Cruzada
class GridSearchCV():
    
    def __init__(self,Input,Output,Semilla): 
        
        self.Model = None    #Modelo que se evaluara
        self.Param = None   #Parametros que se evaluaran
        self.NP = 5         #Numero de pliegues en el modelo
        self.Input = Input   #Input de datos
        self.Output = Output #Output de datos
        self.Semilla = Semilla

        
    def CalcCombi(self):
        #Se calcula los elementos que tendra cada pligue 
        Taminst = int(len(self.Input)/self.NP)  #Tamaño de cada pliegue 
        Inst = np.ones(self.NP)*Taminst    #Se le asigna un numero de elementos a cada pliegue
        Falt = len(self.Input)-Taminst*self.NP  #Se calculan los elementos faltantes
        self.Limits = np.zeros(len(Inst)+1)     #Estos son los limites para cada pliegue
        for i in range(Falt): Inst[i] += 1 #Reparto los elementos faltantes entre los pliegues
        for i in range(1,len(self.Limits)): self.Limits[i] = self.Limits[i-1]+Inst[i-1] #Estos son los limites que seguira la sucesion 
        
        #Obtengo todas las combinaciones de un diccionario
        keys = self.Param.keys()
        values = (self.Param[key] for key in keys)
        self.combinations = [dict(zip(keys, self.Param)) for self.Param in itertools.product(*values)]
        self.CombiA = self.combinations[0]
        #print(self.CombiA)
        #print (self.combinations)
        #print( combinations[0].keys())
      
        
    
    #En este metodo realizo la revicion de la validacion cruzada
    def Ejecucion(self):
        
        self.Inicializacion()
        self.CalcCombi()
        #Para todas las combinaciones de hiperparametros
        for j in range(len(self.combinations)):
            
            self.CombiA =  self.combinations[j]
            ECM = 0

            #Para todas las combinaciones de pliegues
            for i in range(self.NP):
                #Guardo una copia de los Datos y genero los sets de entrenamiento y prueba
                AuxInput = np.copy(self.Input)
                AuxOutput = np.copy(self.Output)
                
                X_Test = np.copy(AuxInput[int(self.Limits[i]):int(self.Limits[i+1]),:])
                X_Train = np.delete(AuxInput,np.s_[int(self.Limits[i]):int(self.Limits[i+1])],0)
                Y_Test = np.copy(AuxOutput[int(self.Limits[i]):int(self.Limits[i+1])])
                Y_Train = np.delete(AuxOutput,np.s_[int(self.Limits[i]):int(self.Limits[i+1])],0)
                
                #Construyo el modelo, lo entreno y evaluo su resultado
                Result = self.BuiltModel(X_Train,Y_Train,X_Test)
                
                ECM += (np.sum(np.sqrt(np.power(Result-Y_Test,2)))/len(Y_Test))

            #Identifico la combinacion con los mejores resultados en la validacion cruzada
            if(j == 0): 
                BestECM = ECM
                Best = 0
            
            else:
                if(BestECM>ECM):
                    BestECM = ECM
                    Best = j
        
        #Reconstruyo el modelo con todos los datos disponibles
        self.CombiA=self.combinations[Best]
        Result = self.BuiltModel(self.Input,self.Output,self.Input)
        
        #Calculo el erro obtenido de los datos
        self.ECM = (np.sum(np.sqrt(np.power(Result-self.Output,2)))/len(self.Output))
        Pen = linear_model.LinearRegression()
        Pen = Pen.fit(Result.reshape(-1, 1),self.Output)
        self.Pendiente  = Pen.coef_[0]
        
        self.BestParam = self.CombiA
        self.BestECM   = self.ECM
        self.BestPend  = self.Pendiente        
    
    #Desde aqui son los metodos que se tiene que modificar para cada modelo
    def Inicializacion(self):

        pass

    #En este metodo construye el modelo con la combinacion A y se entrena
    def BuiltModel(self,X,Y,X_Test):
        
        pass

    def Evaluacion(self,Individuo):
        pass
       
        
        
###############################################################################################       

'''
def Esfera(Input):
    
    Out = np.sum(np.power(Input,2),axis = 1)
    #Out = np.reshape(Out,(len(Input),1))
    return Out

#Creo el conjunto de prubea
Input  = np.random.rand(10,3)     #Genero los valores de entrada
Output = Esfera(Input)           #Genero los puntos de salida


Prue = GridSearchCV(Input,Output,1)
Prue.Ejecucion()
print(Prue.CombiA)
print(Prue.ECM)
print(Prue.Evaluacion([0,1,1]))


#print(Input)
#print(Output)

'''