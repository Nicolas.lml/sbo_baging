
'''
    Este codigo contiene el algoritmo MBO enfocado a resolver el problema de los subconjuntos usando el problema de la mochila 

'''


import MBO 
import numpy as np


#Esta clase resuelve una instancia del problema de la mochila 
class Subset_MBO(MBO.MBO):
    
    #Esta funcion contiene la funcion real de evaluacion del problema
    def Evaluacion_Real(self,Individuo):
        #Aqui se calcula la suma del subconjunto
        valor = 0
        
        for i in range(self.NumDim):
            if(Individuo[i]==1):
                valor += self.Datos[i][0]
        
        #RCalculo la diferencia obtenida con respecto al optimo esperado
        Dif = np.abs(self.ValObj - valor)
        
        return Dif  
    
    #En este metodo se inicializan la informacion faltante del problema de la mochila
    def Inicializacion(self,Datos,ValObj,Modelos):
        self.Datos = Datos
        self.ValObj   = ValObj
        self.NumDim = len(self.Datos)
        self.Dominio = [0,1]
        self.Modelos = Modelos
        self.NumPob = 100
        self.NumGen = 100
        self.Modelos = Modelos
        
        #Desde aqui
        #Se crea una primera poblacion aleatoria con la que se entrenara el modelo
        np.random.seed(self.Semilla)

        self.Input = np.random.randint(self.Dominio[0],self.Dominio[1]+1,(self.PobInc,self.NumDim))
 

        for i in range(self.PobInc):
            self.OutPut.append(self.Evaluacion_Real(self.Input[i]))



'''
np.random.seed(1)


Semilla = 1
NumGen = 100
NumPob = 100

NumDim = 10
Dominio = [0,10]

PobInc = 10

Input = np.random.randint(Dominio[0],Dominio[1],(PobInc,NumDim))
Output = np.sum(np.power(Input,2),axis=1)
TypeMod = ["DTree"]
#Se inicializa la clase
#               Type ,NumPob,NumGen,NumDim,Dominio,Semilla, Modelos,Input,Output
Prueba = Esfera("Dis",NumPob,NumGen,NumDim,Dominio,Semilla, TypeMod,Input,Output)
Prueba.Optimizacion()
print("El optimo es: ",Prueba.BestFit)
print(Prueba.Limpios)
print(Prueba.Fitness)
#print(Prueba.Poblacion)
'''