'''
    El el codigo contiene una clase que uitiliza un algoritmo genetico para optmizar un modelo basado en
    Arboles de Decicion   
'''


'''
    Jefe Fabre

        Comentarios 
            Detalles de los arboles de decicion usadas: https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeRegressor.html?highlight=regressor#sklearn.tree.DecisionTreeRegressor 
                
        Hiperparametros
            'max_depth' : [5,10,15,20, None] #Esperimentacion            
        
        Librerias
            GridSearch: https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html
            Valores Negados: https://living-sun.com/es/python/717902-is-sklearnmetricsmean_squared_error-the-larger-the-better-negated-python-scikit-learn-metrics-mean-square-error.html

'''

from sklearn.tree import DecisionTreeRegressor
import GridSearch
import AG 


#Creo el objeto que hara hara le exploracion exhaustiva para encontrar los mejores parametros usando validacion cruzada
class GS_DTree(GridSearch.GridSearchCV):
    
    #En este metodo construye el modelo y retorna la prediccion asocidad usando la combinavion CombiA
    def BuiltModel(self,X,Y,X_Test):
        
        self.Model = DecisionTreeRegressor(
                max_depth = self.CombiA['max_depth'],
                max_features = self.CombiA['max_features']
                )
        
        #Entreno el modelo
        self.Model.fit(X,Y)
        
        return self.Model.predict(X_Test)

#Creo un objeto que realize la optimizacion interna utilizando un algoritmo genetico
class DTree_AG(AG.AG):

    #Este metodo contiene la informacion de entrenamiento con la que se crea el modelo
    def Inicializacion(self,Input,Output,NumDim):
        
        #Inicializo el modelo que utilizare
        self.Modelo =   DecisionTreeRegressor(max_depth=2)
        
        #Parametros que evaluare en la busqueda exhaustiva
        Param = {
            'max_depth' : [10,20,None],
             'max_features' : ["sqrt", None]
        } 

        NumP = 5 #Numero de pliegues para la validacion cruzada
       
        GS = GS_DTree(Param,NumP,Input,Output,self.Semilla)
        GS.Ejecucion()        
        self.Modelo = GS.Model

        self.BestParam = GS.CombiA
        self.BestECM   = GS.ECM
        self.BestPend  = GS.Pendiente
        

        #print("ECM: ",GS.ECM, " Pend: ",GS.Pendiente," Parametros: ",GS.CombiA)


    #Este es el metodo para evaluar un candidato con el modelo que se este trabajando
    def Evaluacion(self,Individuo):
        
        #Aqui se calcula el fitness de combinacion. 
        Exit = self.Modelo.predict([Individuo])
        return Exit[0]
    

'''
#################################################################################################

import pandas as pd

#Abro el archivo con las caracteristicas del problema a resolver
Archivo = "KP1.csv"
Objetos = np.asarray(pd.read_csv(Archivo))

Exper = []

for i in range(1):
    
    #Creo el modelo con el se trabajara 
    #                     NumPob ,NumGen  ,NumDim      ,Dominio ,Semilla
    Modelo = Knap_RedN_AG(200    ,100     ,len(Objetos),[0,1]   ,i)
    
    #Creo el objeto que realiza la la optimizacion basa en modelos 
    #               PobInc,NumEva,Semilla , Modelo   
    Pro1 = KMBO.MBO_Knap(20    ,25    , i      , Modelo)
    #               Objetos,Capacidad
    Pro1.Inicio_MBO(Objetos,970)
    Pro1.Opimizacion_MBO()
    Pro1.Registro.append(Pro1.CalcPeso(Pro1.SolInd))
    Exper.append(Pro1.Registro)
    #print(Exper[-1])

#Guardo la informacion en un archivo del tipo csv
Info = pd.DataFrame(Exper)
#Info.to_csv("MBO_K6_200"+Archivo+".csv")
'''